import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpResponse, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  getLinkedinAccessToken(code, client_id, client_secret){
    const redirectUrl = 'http://localhost:4200/login'
    debugger
    let body = {
      accessCode: code,
      clientId: client_id,
      clientSecret: client_secret
    }
    const headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*' });
    return this.http.post(environment.serviceUrl + 'linkedin/login', body);
    // return this.http.get(environment.serviceUrl + 'linkedin/demo');
  }

  getLinkedinProfile(){
    let body = {
      accessToken: localStorage.getItem("access_token_linkedin")
    }
    return this.http.post(environment.serviceUrl + 'linkedin/profile', body);
  }

  getLinkedinEmailAddress(){
    let body = {
      accessToken: localStorage.getItem("access_token_linkedin")
    }
    return this.http.post(environment.serviceUrl + 'linkedin/get/email-address', body);
  }
}
