import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LoginService } from './login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  client_id = "77nktjyz4mk457";
  client_secret = "UE6ki4wO980kvzjA";

  constructor(private route: ActivatedRoute, private loginService: LoginService, private router: Router){

  }

  ngOnInit(){

    this.route.queryParams.subscribe(routeParams => {
      const code = routeParams['code'];
        this.loginService.getLinkedinAccessToken(code,this.client_id,this.client_secret).subscribe((res: any) => {
          if(res){
            localStorage.setItem("access_token_linkedin", res.access_token);
            this.router.navigateByUrl('/dashboard');
          }
        },(err) =>{
        })
      },(err) => {

      })
      // this.subscription = this.service.GetSpecificMealsofRestaurant(id).subscribe(res => {
      //     this.item = res;
      //    if(res.status == 500){
      //     this._flashMessagesService.show(res.error.message, { cssClass: 'alert-danger', timeout: 1500 })
      //   }
      // })

  }

  signInWithLinkedin(){
    const redirectUrl = 'http://localhost:4200/login';
    window.open(`https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${this.client_id}&redirect_uri=${redirectUrl}&scope=r_liteprofile r_emailaddress`);
  }

 }
