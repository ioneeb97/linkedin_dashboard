import { Component, OnInit } from '@angular/core';
import { LoginService } from './../login/login.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  public data = {
    username: null,
    firstName: null,
    lastName: null,
    profilePicture: null,
    email: "NA"
  }

  constructor(private service: LoginService){

  }

  ngOnInit(){
    this.service.getLinkedinProfile().subscribe((res: any) => {
      if(res){
        this.data.firstName = res.firstName.localized.en_US.toUpperCase();
        this.data.lastName = res.lastName.localized.en_US.toUpperCase();
        this.data.profilePicture = res.profilePicture["displayImage~"].elements[3].identifiers[0].identifier;
        debugger
        // this.data.username = res.localizedFirstName + " " +  res.localizedLastName;
      }
    },(err) => {

    });

    this.service.getLinkedinEmailAddress().subscribe((res: any) => {
      if(res){
        this.data.email = res.elements[0]["handle~"].emailAddress;
        this.data.username = this.data.email.split('@')[0];
        // this.data.username = res.localizedFirstName + " " +  res.localizedLastName;
      }
    },(err) => {

    });
  }
}
